package util;

 
public class Query {

public static final String ADD_CUSTOMER = "insert into customer values(?,?,?,?,?,?,?)";
public static final String DELETE_CUSTOMER = "delete from customer where id=?";
public static final String UPDATE_CUSTOMER = "update customer set name=?, book=?, price=?, mobile=?, email=?, address=? where id=?";
public static final String VIEW_CUSTOMER = "select * from customer";

public static final String VALIDATE_ADMIN = "select * from admin where username=? and password=?";
public static final String VALIDATE_USER = "select * from user where username=? and password=?";
public static final String REGISTER_USER = "insert into user values(?,?,?)";
public static final String VIEW_ALL_USERS = "select * from user";

 

}
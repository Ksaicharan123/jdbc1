package controller;

import java.util.List;
import dao.IUser;
import dao.UserImpl;
import model.User;

 public class UserController {
    static User user;
    static IUser impl = new UserImpl();

 public String userLoginValidation(String username, String password) {
        user = new User();
        user.setUsername(username);
        user.setPassword(password);
        return impl.userLoginValidation(user);
    }

 public String addUser(String username, String password, long mobile) {
        user = new User(username,password,mobile);
        return impl.addUser(user);
    }
    public List<User> viewUser() {
        return impl.viewUser();
    }
}
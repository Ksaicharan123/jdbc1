package controller;

 

import java.util.List;
import dao.ICustomer;
import dao.CustomerImpl;
import model.Customer;

 

public class CustomerController {
	
	Customer customer;
    CustomerImpl impl = new CustomerImpl();

    public String addCustomer(Integer id, String name, String book, Double price, String mobile, String email, String address ) {
        customer = new Customer(id, name, book, price, mobile, email, address);
        return impl.addCustomer(customer);
    }

    public List<Customer> viewCustomer() {
        return impl.viewCustomer();
    }

    public String updateCustomer(Integer id, String name, String book, Double price, String mobile, String email, String address) {
        customer = new Customer(id, name, book, price, mobile, email, address);
        return impl.updateCustomer(customer);
    }

    public String deleteCustomer(Integer id) {
        customer = new Customer();
        customer.setId(id);;
        return impl.deleteCustomer(customer);
    }

 

}



package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

 

import model.Customer;
import util.Db;
import util.Query;

 

public class CustomerImpl implements ICustomer {
    PreparedStatement statement;
    String result;

 

    public String addCustomer(Customer customer) {
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.ADD_CUSTOMER);
            statement.setInt(1, customer.getId());
            statement.setString(2, customer.getName());
            statement.setString(3, customer.getBook());
            statement.setDouble(4, customer.getPrice());
            statement.setString(5, customer.getMobile());
            statement.setString(6, customer.getEmail());
            statement.setString(7,  customer.getAddress());
            statement.executeUpdate();
            result = "Inserted Successfully";
        } catch (SQLException ex) {
            result = "Duplicate Exists";
        }
        return result;
    }

 

    public List<Customer> viewCustomer() {
        List<Customer> list = new ArrayList<Customer>();
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.VIEW_CUSTOMER);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                Customer customer = new Customer(set.getInt(1), set.getString(2), set.getString(3), set.getDouble(4), set.getString(5),
                        set.getString(6), set.getString(7));
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return list;
    }

 

    public String updateCustomer(Customer customer) {
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.UPDATE_CUSTOMER);
            statement.setInt(7, customer.getId());
            statement.setString(1, customer.getName());
            statement.setString(2, customer.getBook());
            statement.setDouble(3, customer.getPrice());
            statement.setString(4, customer.getMobile());
            statement.setString(5, customer.getEmail());
            statement.setString(6, customer.getAddress());
            
            int status = statement.executeUpdate();
            if (status > 0) {
                result = "Update Sucessfully";
            } else {
                result = "Record Not Found";
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return result;
    }

 

    public String deleteCustomer(Customer customer) {
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.DELETE_CUSTOMER);
            statement.setInt(1, customer.getId());
            int status = statement.executeUpdate();
            if (status > 0) {
                result = "Deleted Sucessfully";
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return result;
    }
}



	

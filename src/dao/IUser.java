package dao;

import java.util.List;
import model.Customer;
import model.User;

public interface IUser {
    public String userLoginValidation(User user);

    public String addUser(User user);

    public List<User> viewUser();

 

}
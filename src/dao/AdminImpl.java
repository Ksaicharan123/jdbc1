package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Admin;
import util.Db;
import util.Query;

public class AdminImpl  implements IAdmin{
    PreparedStatement statement;
    String result;
    public String adminLogValidation(Admin admin) {
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.VALIDATE_ADMIN);
            statement.setString(1, admin.getUsername());
            statement.setString(2, admin.getPassword());
            ResultSet rs = statement.executeQuery();
            int temp=0;
            while(rs.next()) {
                temp++;
            }
            if(temp>0) {
                result="Welcome to Admin page";
            }
            else
            {
                System.out.println("Username or Password is Incorrect");
                System.exit(0);
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }
        return "Welcome to Admin page";    
    }    
}
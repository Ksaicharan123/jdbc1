package dao;

 

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Customer;
import model.User;
import util.Db;
import util.Query;

 public class UserImpl implements IUser {
    PreparedStatement statement;
    String result;

    @Override
    public String userLoginValidation(User user) {
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.VALIDATE_USER);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            ResultSet rs = statement.executeQuery();
            int temp = 0;
            while (rs.next()) {
                temp++;
            }
            if (temp > 0) {
                result = "Welcome To UserPage";
            } else {

                System.out.println("Username or Password is Incorrect.");
                System.exit(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String addUser(User user) {
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.REGISTER_USER);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setLong(3, user.getMobile());

 

            int status = statement.executeUpdate();
            if (status > 0) {
                result = "Registered Successfully";
            } else {
                result = "please try again!";
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

 
   @Override
    public List<User> viewUser() {
        List<User> list = new ArrayList<User>();
        try {
            statement = Db.getObject().getConnection().prepareStatement(Query.VIEW_ALL_USERS);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                User user = new User( set.getString(1),set.getString(2),set.getInt(3));
                list.add(user);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return list;

    }

 

}
package dao;

import java.util.List;
import model.Customer;

public interface ICustomer {
	
	public String addCustomer(Customer customer);

	 

    public List<Customer> viewCustomer();

 

    public String updateCustomer(Customer customer);

 

    public String deleteCustomer(Customer customer);

}

package model;

public class User {
    private String username;
    private String password;
    private long mobile;
    public User() {
        // TODO Auto-generated constructor stub
    }

    public User(String username, String password, long mobile) {
        super();
        this.username = username;
        this.password = password;
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public long getMobile() {
        return mobile;
    }
    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }
    @Override
    public String toString() {
        return String.format("%-20s%-20s%-20s", username,password,mobile);
    }
}
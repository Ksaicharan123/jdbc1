package model;
public class Customer {

private int id;
private String name;
private String book;
private double price;
private String mobile;
private String email;
private String address;

 
public Customer() {

 

}


public Customer(int id, String name, String book, double price, String mobile, String email, String address) {
	super();
	this.id = id;
	this.name = name;
	this.book = book;
	this.price = price;
	this.mobile = mobile;
	this.email = email;
	this.address = address;
}


public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public String getBook() {
	return book;
}


public void setBook(String book) {
	this.book = book;
}


public double getPrice() {
	return price;
}


public void setPrice(double price) {
	this.price = price;
}


public String getMobile() {
	return mobile;
}


public void setMobile(String mobile) {
	this.mobile = mobile;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public String getAddress() {
	return address;
}


public void setAddress(String address) {
	this.address = address;
}


@Override
public String toString() {
	 return String.format("%-20s%-20s%-20s%-20s%-20s%-20s%-20s", id, name, book,  price, mobile, email, address);
}



}



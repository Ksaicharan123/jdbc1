package model;

public class Admin {
    private String username;
    private String password;
    public Admin() {

    }
    public Admin(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public String toString() {
        return String.format("%-20s%-20s", username,password);
    }

 

}
package view;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;
import controller.AdminController;
import controller.CustomerController;
import controller.UserController;
import model.Customer;

 public class Main {

	 static String s[];

 public static void main(String[]args) throws ClassNotFoundException, SQLException {
Scanner scanner = new Scanner(System.in);
System.out.println("---------------------------------------WELCOME TO CUSTOMER ADDRESS BOOK-----------------------------------");
System.out.println("Press 1 for Admin and 2 for User");
int option = scanner.nextInt();

if(option==1) {
scanner.nextLine();
AdminController adminController = new AdminController();
System.out.println("Enter username");
String username=scanner.nextLine();
System.out.println("Enter password");
String password=scanner.nextLine();
System.out.println(adminController.adminLogValidation(username, password));
CustomerController controller = new CustomerController();

try {

int wouldYouLikeToContinue;

do {

 System.out.println(" MENU: 1.Add Customer 2. Delete Customer 3.Update Customer 4.View Customer");

int choice = scanner.nextInt();

switch (choice) {

case 1:

scanner.nextLine();
System.out.println("Enter Customer ID,Name,Book,Price,Mobile,Email,Address for ADD");
s = scanner.nextLine().split(",");
System.out.println(controller.addCustomer(Integer.parseInt(s[0]), s[1], s[2],(double) Integer.parseInt( s[3]), s[4], s[5], s[6]));
break;
case 2:
System.out.println("Enter Customer ID for DELETE");
Integer id = scanner.nextInt();
System.out.println(controller.deleteCustomer(id));
break;

case 3:
scanner.nextLine();
System.out.println("Enter Customer ID, Name, Book, Price, Mobile, Email, Address for Update");
s = scanner.nextLine().split(",");
System.out.println(controller.updateCustomer(Integer.parseInt(s[0]), s[1], s[2],(double) Integer.parseInt( s[3]), s[4], s[5], s[6]));
break;

case 4:
System.out.format("%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n", "CUSTOMER_ID", "CUSTOMER_NAME", "CUSTOMER_BOOK", "CUSTOMER_PRICE", "CUSTOMER_MOBILE", "CUSTOMER_EMAIL", "CUSTOMER_ADDRESS");
for (Customer i : controller.viewCustomer()) {
System.out.println(i);
}

}
System.out.println("\nDo you want to continue User Operations?if yes - press 1,else to Logout press 0");
wouldYouLikeToContinue=scanner.nextInt();
} while(wouldYouLikeToContinue==1);
System.out.println("Thank You Sir..!");
} catch (InputMismatchException | NullPointerException ex) {
System.out.println(ex.getMessage());
}
}

else if(option==2) {
	
	System.out.println("Press 1 for Login, press 2 for signup");
	option = scanner.nextInt();
	if(option == 1){
		scanner.nextLine();
		UserController userController = new UserController();
		System.out.println("Enter username:");
		String username = scanner.nextLine();
		System.out.println("Enter password");
		String password = scanner.nextLine();
		
		System.out.println(userController.userLoginValidation(username, password));
		CustomerController controller = new CustomerController();
		try {
			
			System.out.println("1.View CUSTOMER");
			int choice = scanner.nextInt();
			System.out.format("%-20s%-20s%-20s%-20s%-20s%-20s%-20s\n", "CUSTOMER_ID", "CUSTOMER_NAME", "CUSTOMER_BOOK", "CUSTOMER_PRICE", "CUSTOMER_MOBILE", "CUSTOMER_EMAIL", "CUSTOMER_ADDRESS");
			for (Customer c : controller.viewCustomer()) {
				System.out.println(c);
			}
			System.out.println("Thank You Sir..!");
			
			} catch (InputMismatchException e) {
				System.out.println(e.getMessage());
		}
	}
else if(option==2) {
	scanner.nextLine();
	UserController userController = new UserController();
	System.out.println("Please provide the necessary information to Register!!");
	System.out.println("Enter username");
	String username = scanner.nextLine();
	System.out.println("Enter password");
	String password = scanner.nextLine();
	System.out.println("Enter Mobile");
	long mobile = scanner.nextLong();
	System.out.println(userController.addUser(username,password,mobile));
	
	
} else {
	System.out.println("Please choose a valid option");
	}
 }

else {
	 System.out.println("please choose a valid option");
 }
 
 scanner.close();
}
 }

